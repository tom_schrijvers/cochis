# COCHIS 

This repository provides a prototype of the COCHIS calculus 
defined in the article

  COCHIS: Stable and Coherent Implicits
  Tom Schrijvers, Bruno C. d. S. Oliveira, Philip Wadler, Koar Marntirosian

## Installation

Build the prototype with 

> ghc --make Main.hs

## Execution

Run the executable on your .coh file, e.g.,

> ./Main examples/example1.coh

## Examples

The examples/ folder contains several example files.

## Note

The parser is not very clever---use plenty of parentheses.
